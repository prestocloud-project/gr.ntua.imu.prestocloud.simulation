set timezone='Europe/Athens';
SELECT
  (occurred at time zone 'Europe/Athens') as time,
  k AS metric,
  (v - (select min(v) from status where k='workload') / (select max(v)- min(v) from stats where k='workload')) * 100
FROM stats
WHERE
  $__timeFilter((occurred at time zone 'Europe/Athens' ))
  and k = 'workload'
ORDER BY 1,2