create table topology_stats(

                             id serial primary key ,
                             fragment VARCHAR(255),
                             cpuLoad float,
                             cpuCapacity float,
                             memoryLoad float,
                             memoryCapacity float,
                             occurred TIMESTAMP

);

