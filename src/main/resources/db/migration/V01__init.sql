create table stats(
                      id serial,
                      k varchar(255),
                      v varchar(255),
                      ret varchar(255),
                      occurred timestamp
);

create unique index stats_pkey ON stats(id,occurred desc);



create table topology(

                             id serial primary key ,
                             fragid VARCHAR(255),
                             type VARCHAR(255),
                             instances INT,
                             updated TIMESTAMP
);



create table annotations(

                            id serial,
                            action VARCHAR(255),
                            text VARCHAR(255),
                            occurred TIMESTAMP


);

SELECT create_hypertable('annotations', 'occurred');
