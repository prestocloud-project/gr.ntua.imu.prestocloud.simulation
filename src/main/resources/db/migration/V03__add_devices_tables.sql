create table devices
(
    id  serial primary key,
    hexId varchar(255),
    type varchar(255),
    ip varchar(255),
    fragment varchar(255),
    instance varchar(255),
    excluded timestamp,
    occurred timestamp

);

