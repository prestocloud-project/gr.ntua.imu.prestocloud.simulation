#!/usr/bin/env python
import pika
import time
import json
import random
import json


SLEEP=.50
BROKER_HOST='rarecom.presto.imuresearch.eu' #okeanos


connection = pika.BlockingConnection(pika.ConnectionParameters(host=BROKER_HOST))
channel = connection.channel()

channel.exchange_declare(exchange='presto.cloud', exchange_type='topic', durable=True)

with open('data.txt') as json_file:
    data = json.load(json_file)
    for p in data['people']:
        print('Name: ' + p['name'])
        print('Website: ' + p['website'])
        print('From: ' + p['from'])
        print('')

somedict ={
    "event": {
        "rule_id": "Xs3UieyyCj",
        "timestamp": 1553785248,
        "res_inst": "",
        "fragid": "KdkCcQkMNe",
        "zone": 1,
        "action": "scale_out",
        "avg_cpu":  92,
        "avg _ram": 71
    }
}




message = json.dumps(somedict)
channel.basic_publish(exchange='presto.cloud',
                      routing_key='situations',
                      body=json.dumps(somedict))


connection.close()
