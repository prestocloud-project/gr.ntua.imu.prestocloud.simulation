#!/usr/bin/env python
import pika
import time
import json
import random



#
#
#########################################################################################
#
#               Change your options here
#
#########################################################################################
#
#

BROKER_HOST='52.58.107.100'
TOPIC="mca.monitoring"

MAP = [
    ('ipv6','responsetime'),
    ('ipv7','responsetime'),
    ('ipv8','responsetime'),
    ('ipv9','responsetime'),
    ('ipv10','responsetime')
]

USE_RANDOM=False
RANDOM_LOWER=10
RANDOM_UPPER=50

SLEEP_IN_SECONDS=1


#
#
#########################################################################################
#
#



connection = pika.BlockingConnection(pika.ConnectionParameters(host=BROKER_HOST))
channel = connection.channel()

channel.exchange_declare(exchange='presto.cloud', exchange_type='topic', durable=True)

for o in MAP:


    response_time =o[1]


    if USE_RANDOM:
        response_time=  random.uniform(RANDOM_LOWER,RANDOM_UPPER)

    event =  [
        {
            "event": {
                "avg_response_time": response_time,
                "fragid": "1cc5Fragment",
                "res_inst": o[0]
            }
        }
    ]

    print(event)
    time.sleep(SLEEP_IN_SECONDS)

    channel.basic_publish(exchange='presto.cloud',
                          routing_key=TOPIC,
                          body=json.dumps(event))


connection.close()
