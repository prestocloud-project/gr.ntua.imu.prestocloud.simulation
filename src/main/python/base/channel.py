#!/usr/bin/env python
import pika

class Channel:

    SLEEP=.50
    BROKER_HOST='10.8.6.20'
    credentials = pika.PlainCredentials('guest', 'guest')

    def send(self, topic, file):

        connection = pika.BlockingConnection(pika.ConnectionParameters(host=self.BROKER_HOST,credentials=self.credentials))
        channel = connection.channel()
        channel.exchange_declare(exchange='presto.cloud', exchange_type='topic', durable=True)

        with open(file, 'r') as jsonFile:

            data = jsonFile.read()
            channel.basic_publish(exchange='presto.cloud',
                                  routing_key=topic,
                                  body=data)

        connection.close()

