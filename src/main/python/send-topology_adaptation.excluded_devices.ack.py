#!/usr/bin/env python
import pika
import time
import json
import random


SLEEP=.50
BROKER_HOST='10.8.6.20'

credentials = pika.PlainCredentials('guest', 'guest')
connection = pika.BlockingConnection(pika.ConnectionParameters(host=BROKER_HOST,credentials=credentials))
channel = connection.channel()

channel.exchange_declare(exchange='presto.cloud', exchange_type='topic', durable=True)


with open('../resources/json/topology_adaptation.excluded_devices.ack.json', 'r') as jsonFile:

    data = jsonFile.read()
    channel.basic_publish(exchange='presto.cloud',
                          routing_key='topology_adaptation.excluded_devices.ack',
                          body=data)


connection.close()
