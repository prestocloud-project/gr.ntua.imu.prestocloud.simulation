package gr.ntua.imu.prestocloud.simulation.services

import gr.ntua.imu.prestocloud.simulation.ampq.SimulationPublisher
import gr.ntua.imu.prestocloud.simulation.configuration.ApplicationConfiguration
import net.andreinc.mockneat.MockNeat
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-12-16.
 */
@Service
class DevicesService {

    Logger logger = LoggerFactory.getLogger(DevicesService.class)

    @Autowired
    ApplicationConfiguration applicationConfiguration

    @Autowired
    WorkloadCalculatorService workloadCalculatorService

    @Autowired
    MockNeat mockNeat

    @Autowired
    JdbcTemplate jdbcTemplate

    SimpleJdbcInsert jdbcInsert


    @PostConstruct
    public post() {

        jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
            .withTableName("devices")
            .usingGeneratedKeyColumns("id")


        jdbcTemplate.update("truncate devices RESTART IDENTITY;")

        applicationConfiguration.devices.each {
            k, device ->

                int p = 0
                while (p < device.pool) {

                    def d = [
                        "hexId"    : mockNeat.hashes().md2().get(),
                        "ip"       : mockNeat.iPv6s().val(),
                        "type"     : k,
                        "cpu"      : device.configuration.cpu,
                        "memory"   : device.configuration.memory,
                        "benchmark": mockNeat.doubles().range(0.1, 1.0).get(),
                        "occurred" : new Date()
                    ]
                    jdbcInsert.execute(d)

                    p++
                }
        }


    }


    public List getDeviceList() {

        return jdbcTemplate.queryForList("select * from devices")
    }

    public List getDeviceList(String type) {

        return jdbcTemplate.queryForList("select * from devices where type=?",type)
    }

    public Object getDevice(String type) {


        def object = null
        try{

            object = jdbcTemplate.queryForMap("select * from devices where type=? AND fragment is null and excluded is null order by benchmark desc limit 1", type)
            logger.debug("I was asked for a device and returned ({}) ==> {}", type)

        }catch(EmptyResultDataAccessException e){
            logger.debug("I was asked for a device ({}) and couldn't find one ", type)

        }
        return object
    }

    public Object unexclude() {

        logger.debug("I am unexcluding devices")
        jdbcTemplate.update("update devices set excluded=null,fragment=null where excluded is not null")
    }

    public Object excludeDevice(String ip) {

        logger.debug("I am excluding device with ip {}", ip)

        def updated = jdbcTemplate.update("update devices set excluded=NOW() where ip=? and fragment IS NOT NULL ", ip)

        if(updated <=0){
            logger.error("\t {} no fragment excluded",ip)
        }
    }


    public Object reserveDevice(int id, String fragment, String instance) {
        jdbcTemplate.update("update devices set fragment=?, instance=? where id=? ", fragment, instance, id)
    }

    public Object releaseDevice(String fragment) {

        def object = jdbcTemplate.queryForMap("select * from devices where fragment=? order by benchmark ASC limit 1", fragment)

        logger.debug("I am releasing device {} ",object)

        if(object==null){
            throw new RuntimeException("I ran out of devices to release")
        }

        jdbcTemplate.update("update devices set fragment=null, instance=null where id=? ", object.id)

    }

    public computeLoad(workload){

        int totalCpu = 0
        int totalMem= 0


        def activeDevices = jdbcTemplate.queryForList(
            "select * from devices where fragment IS NOT NULL"
        )

        activeDevices.each {
            f->
                totalCpu += f.cpu
                totalMem += f.memory
        }


        def currentCPULoad=0
        def currentMemLoad=0

        def currentCPUCapacity=0
        def currentMemCapacity=0

        def devices = []
        activeDevices.each {
            v ->

                def fragment = applicationConfiguration.topology.get(v.fragment)
                currentCPULoad += workloadCalculatorService.currentCpuLoad(workload,totalCpu,fragment)
                currentMemLoad += workloadCalculatorService.currentMemLoad(workload,totalMem,fragment)

                currentCPUCapacity += workloadCalculatorService.currentCpuCapacity(totalCpu)
                currentMemCapacity += workloadCalculatorService.currentMemCapacity(totalMem)


                def device = applicationConfiguration.devices.get(v.type)

                def cpuRatio = v.cpu / totalCpu
                def memRatio = v.memory / totalMem

                def cpuLoad = cpuRatio *currentCPULoad
                def cpuCapacity= cpuRatio *currentCPUCapacity

                def memoryLoad= memRatio*currentMemLoad
                def memoryCapacity= memRatio*currentMemCapacity

                v["responseTime"] = (1/v.benchmark * cpuLoad) + (1/v.benchmark * memoryLoad)
                v["cpuLoad"] = cpuLoad
                v["cpuCapacity"] = cpuCapacity
                v["memoryLoad"] = memoryLoad
                v["memoryCapacity"] = memoryCapacity

                devices.add(v)

        }


        return [
            "cpuLoad" :currentCPULoad,
            "memoryLoad" :currentMemLoad,
            "cpuCapacity" :currentCPUCapacity,
            "memoryCapacity" :currentMemCapacity,
            "devices" : devices
        ]

    }

}
