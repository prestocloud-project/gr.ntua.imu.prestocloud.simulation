package gr.ntua.imu.prestocloud.simulation.ampq

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel
import com.rabbitmq.client.DefaultConsumer
import com.rabbitmq.client.Envelope
import gr.ntua.imu.prestocloud.simulation.services.ActionHandlingService

public class SimulationReceiver extends DefaultConsumer{


    private final ActionHandlingService actionHandlingService


    Gson gsonizer

    /**
     * Constructs a new instance and records its association to the passed-in channel.
     * @param channel the channel to which this consumer is attached
     */
    SimulationReceiver(Channel channel, ActionHandlingService actionHandlingService) {
        super(channel)

        gsonizer = new GsonBuilder().create()
        this.actionHandlingService = actionHandlingService



    }
    @Override
    void handleDelivery(String consumerTag, Envelope envelope,
                               AMQP.BasicProperties properties, byte[] body) throws IOException {


        def o = gsonizer.fromJson(new String(body, "UTF-8"),Object.class)
        actionHandlingService.handleMessage([
            topic: envelope.routingKey,
            payload: o]
        )



    }

}