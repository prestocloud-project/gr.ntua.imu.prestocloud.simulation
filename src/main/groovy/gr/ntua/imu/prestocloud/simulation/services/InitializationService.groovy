package gr.ntua.imu.prestocloud.simulation.services

import gr.ntua.imu.prestocloud.simulation.ampq.SimulationPublisher
import org.apache.commons.io.IOUtils
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2020-01-03.
 */
@Service
class InitializationService {

    @Value('${application.rarecomKey}')
    String rarecomKey

    @Value('${application.time.policyPeriodInSeconds}')
    long policyPeriod


    @Value('${application.time.inertialPeriodInSeconds}')
    long inertialPeriod

    @Autowired
    SequenceStepService sequenceStepService

    @Autowired
    SimulationPublisher simulationPublisher


    @Autowired
    DevicesService devicesService

    @PostConstruct
    def post(){

    }

    public void init(){

        simulationPublisher.publish('undeployment.ack',['graphInstanceHexID':'ubBFY9f3e0'])

        Thread.sleep(5000)

        def devices = devicesService.getDeviceList('edge')
        devices.each{
            i ->

                Random r = new Random()

                simulationPublisher.publish(
                    'edge.benchmark.results',
                    [
                        "fragid": "edge-benchmark-v3",
                        "res_inst": i.ip,
                        "task_exec_time_ms": r.nextInt((300 - 50) + 1) + 50
                    ]

                )
        }
        sequenceStepService.nextStep()

        def deployment_iput= IOUtils.toString(SimulationPublisher.class.getResourceAsStream("/json/deployment.topology_input.json"),'UTF-8')
        JSONParser parser = new JSONParser();
        def o = (JSONObject) parser.parse(deployment_iput);
        simulationPublisher.publish("deployment.topology_input",o)

        while(sequenceStepService.currentStep() < SequenceStepService.STEP_READY){

            if(sequenceStepService.isStep(SequenceStepService.STEP_INITIAL_DEPLOYMENT)){
                simulationPublisher.publish("deployment.ack", new Object())
                sequenceStepService.nextStep()
            }

            if(sequenceStepService.isStep(SequenceStepService.STEP_POLICY)){
                simulationPublisher.publish('policies.cruder',
                    [

                        "callbackURL"        : "https://jsonplaceholder.typicode.com/posts",
                        "graphHexID"         : "F1sJQuetaU",
                        "graphInstanceHexID" : "ubBFY9f3e0",
                        "policyHexID"        : "Xs3UieyyCn",
                        "expressionModelList": [
                            [
                                "function"      : "AVG",
                                "componentHexID": "MN5S6NmR1F",
                                "componentName" : "VideoTranscoder",
                                "metric"        : "cpu_perc",
                                "operand"       : "GREATER",
                                "threshold"     : "80"
                            ],
                            [
                                "function"      : "AVG",
                                "componentHexID": "MN5S6NmR1F",
                                "componentName" : "VideoTranscoder",
                                "metric"        : "mem_perc",
                                "operand"       : "GREATER",
                                "threshold"     : "60"
                            ],

                        ],
                        "policyPeriod"       : policyPeriod,
                        "inertialPeriod"       : inertialPeriod,
                        "actionModelsList"   : [
                            [
                                "context"    :  "1",
                                "ruleAction"    : "scaleOut",
                                "componentName" : "VideoTranscoder",
                                "componentHexID": "MN5S6NmR1F",
                                "topicUrl": "",
                                "topicName": "",
                                "topicPort": ""

                            ]
                        ],
                        "creation"           : true
                    ]
                )

                simulationPublisher.publish('policies.cruder',
                    [

                        "callbackURL"        : "https://jsonplaceholder.typicode.com/posts",
                        "graphHexID"         : "F1sJQuetaY",
                        "graphInstanceHexID" : "ubBFY9f3e0",
                        "policyHexID"        : "Xs3UieyyCj",
                        "expressionModelList": [
                            [
                                "function"      : "AVG",
                                "componentHexID": "MN5S6NmR1F",
                                "componentName" : "VideoTranscoder",
                                "metric"        : "cpu_perc",
                                "operand"       : "LESS",
                                "threshold"     : "50"
                            ],
                            [
                                "function"      : "AVG",
                                "componentHexID": "MN5S6NmR1F",
                                "componentName" : "VideoTranscoder",
                                "metric"        : "mem_perc",
                                "operand"       : "LESS",
                                "threshold"     : "40"
                            ],

                        ],
                        "policyPeriod"       : policyPeriod,
                        "inertialPeriod"       : inertialPeriod,
                        "actionModelsList"   : [
                            [
                                "context"    :  "1",
                                "ruleAction"    : "scaleIn",
                                "componentName" : "VideoTranscoder",
                                "componentHexID": "MN5S6NmR1F",
                                "topicUrl": "",
                                "topicName": "",
                                "topicPort": ""
                            ]
                        ],
                        "creation"           : true
                    ]
                )

                simulationPublisher.publish('policies.cruder',
                    [

                        "callbackURL"        : "https://jsonplaceholder.typicode.com/posts",
                        "graphHexID"         : "F1sJQuetaY",
                        "graphInstanceHexID" : "ubBFY9f3e0",
                        "policyHexID"        : "Xs3UieyyBn",
                        "expressionModelList": [
                            [
                                "function"      : "AVG",
                                "componentHexID": "MN5S6NmR1H",
                                "componentName" : "PercussionDetector",
                                "metric"        : "cpu_perc",
                                "operand"       : "GREATER",
                                "threshold"     : "90"
                            ],
                            [
                                "function"      : "AVG",
                                "componentHexID": "MN5S6NmR1H",
                                "componentName" : "PercussionDetector",
                                "metric"        : "mem_perc",
                                "operand"       : "GREATER",
                                "threshold"     : "90"
                            ],

                        ],
                        "policyPeriod"       : policyPeriod,
                        "inertialPeriod"       : inertialPeriod,
                        "actionModelsList"   : [
                            [
                                "context"    :  "1",
                                "ruleAction"    : "scaleOut",
                                "componentName" : "PercussionDetector",
                                "componentHexID": "MN5S6NmR1H",
                                "topicUrl": "",
                                "topicName": "",
                                "topicPort": ""
                            ]
                        ],
                        "creation"           : true
                    ]
                )

                simulationPublisher.publish('policies.cruder',
                    [

                        "callbackURL"        : "https://jsonplaceholder.typicode.com/posts",
                        "graphHexID"         : "F1sJQuetaY",
                        "graphInstanceHexID" : "ubBFY9f3e0",
                        "policyHexID"        : "Xs3UieyyCk",
                        "expressionModelList": [
                            [
                                "function"      : "AVG",
                                "componentHexID": "MN5S6NmR1G",
                                "componentName" : "FaceDetector",
                                "metric"        : "cpu_perc",
                                "operand"       : "LESS",
                                "threshold"     : "20"
                            ],
                            [
                                "function"      : "AVG",
                                "componentHexID": "MN5S6NmR1G",
                                "componentName" : "FaceDetector",
                                "metric"        : "mem_perc",
                                "operand"       : "LESS",
                                "threshold"     : "10"
                            ],

                        ],
                        "policyPeriod"       : policyPeriod,
                        "inertialPeriod"       : inertialPeriod,
                        "actionModelsList"   : [
                            [
                                "context"    :  "1",
                                "ruleAction"    : "scaleIn",
                                "componentName" : "FaceDetector",
                                "componentHexID": "MN5S6NmR1G",
                                "topicUrl": "",
                                "topicName": "",
                                "topicPort": ""
                            ]
                        ],
                        "creation"           : true
                    ]
                )

                sequenceStepService.nextStep()
                sequenceStepService.nextStep()
            }

        }
    }
}
