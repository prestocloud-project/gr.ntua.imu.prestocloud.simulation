package gr.ntua.imu.prestocloud.simulation.processors


import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-03-13.
 */
@Component
class StoreCPUProcessor {


  Logger logger = LoggerFactory.getLogger(StoreCPUProcessor.class)
  @Autowired
  JdbcTemplate jdbcTemplate
  SimpleJdbcInsert jdbcInsert

  def instance

  @PostConstruct
  public void post(){

    jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
        .withTableName("stats")
        .usingGeneratedKeyColumns("id")
    instance = Random.newInstance()
  }


  void process() throws Exception {

    def i= [
        "k":"cpu",
        "v": instance.nextInt(4)+1,
        "occurred": new Date(),
        "ret":"stats-cpu-random"
    ]

    jdbcInsert.execute(i)
    logger.debug("Inserting {} ",i);

  }
}
