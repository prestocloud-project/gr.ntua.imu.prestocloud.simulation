package gr.ntua.imu.prestocloud.simulation.processors

import gr.ntua.imu.prestocloud.simulation.ampq.SimulationPublisher
import gr.ntua.imu.prestocloud.simulation.configuration.ApplicationConfiguration
import gr.ntua.imu.prestocloud.simulation.services.DevicesService
import gr.ntua.imu.prestocloud.simulation.services.SequenceStepService
import gr.ntua.imu.prestocloud.simulation.services.WorkloadCalculatorService
import gr.ntua.imu.prestocloud.simulation.workload.AppleWorkload
import net.andreinc.mockneat.MockNeat
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-03-13.
 */
@Component
class ComputeCurrentWorkloadProcessor {

    @Autowired
    ApplicationConfiguration applicationConfiguration

    @Autowired
    DevicesService deviceService

    Logger logger = LoggerFactory.getLogger(ComputeCurrentWorkloadProcessor.class)

    @Autowired
    SimulationPublisher simulationPublisher

    @Autowired
    JdbcTemplate jdbcTemplate

    SimpleJdbcInsert jdbcInsert

    @Autowired
    SequenceStepService sequenceStepService

    @Autowired
    WorkloadCalculatorService workloadCalculatorService

    List availableWorkload
    def stat


    @PostConstruct
    public void post() {


        jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
            .withTableName("stats")
            .usingGeneratedKeyColumns("id")


        jdbcInsert.execute([
            "k"       : "cpu",
            "v"       : 1,
            "occurred": new Date(),
            "ret"     : "stats-cpu-${stat}"
        ])

        jdbcInsert.execute([
            "k"       : "memory",
            "v"       : 2,
            "occurred": new Date(),
            "ret"     : "stats-mem-${stat}"
        ])

        def workloadAdapter = new AppleWorkload(applicationConfiguration.adapterMultiplier)
        availableWorkload = workloadAdapter.workload()

    }


    void process() throws Exception {

        if (sequenceStepService.currentStep() < SequenceStepService.STEP_READY){
            logger.debug("We are not ready yet {} ",sequenceStepService.currentStep())
            return;
        }

        if(availableWorkload.size() <= 0){
            logger.info("Done!")
            System.exit(0)
        }

        def workload = availableWorkload.get(0)
        logger.debug("Available workload left {} ",availableWorkload.size())


        def activeDevices = deviceService.computeLoad(workload)

        def i = [
            "k"       : "workload",
            "v"       : workload,
            "occurred": new Date(),
            "ret"     : "stats-${stat}"
        ]

        jdbcInsert.execute(i)

        def load =  activeDevices.cpuLoad+activeDevices.memoryLoad

        Map<String,Object> loadPerFragment = new HashMap<>()


        activeDevices.devices.each{

            device ->
                simulationPublisher.publish(
                    "monitoring.${applicationConfiguration.graph.hexID}.${applicationConfiguration.graph.instanceHexID}.${device.type}.${device.fragment}.${device.instance}",
                    [
                        "disk_available": 0,
                        "res_inst": device.ip,
                        "net_sent": 0,
                        "loadBalancerMonitoringModels": [
                            [
                                "linvocations_sec": 0,
                                "loadbalancer_for_fragid": "",
                                "linvocations_responsetime": 0
                            ]
                        ],
                        "cpu_user": 0,
                        "cpu_io_wait": 0,
                        "throughput_rate_fps": 0,
                        "latency_ms": 0,
                        "fragid": device.fragment,
                        "cpu_sys": 0,
                        "mem_perc": device.memoryLoad,
                        "cpu_perc": device.cpuLoad,
                        "packets_sent": 0,
                        "mem_free": 0,
                        "packets_received": 0,
                        "net_rec": 0,
                        "response_time_ms": device.responseTime, //TODO nikos curves
                        "timestamp": new Date().time
                    ]

                )


                if (!loadPerFragment.containsKey(device.fragment)){
                    loadPerFragment.put(device.fragment,[
                        "load" :0.0,
                        "count" : 0
                    ])
                }


                def fragmentStats = loadPerFragment.get(device.fragment)
                fragmentStats.load  +=device.cpuLoad + device.memoryLoad
                fragmentStats.count++

                Thread.sleep(500)
        }


        loadPerFragment.each{
            k , v ->

                jdbcInsert.execute([
                    "k"       : "load-"+k,
                    "v"       : v.load / v.count,
                    "occurred": new Date(),
                    "ret"     : "stats-fragment-load-${stat}"
                ])


        }

        jdbcInsert.execute([
            "k"       : "load",
            "v"       : load,
            "occurred": new Date(),
            "ret"     : "stats-load-${stat}"
        ])


        jdbcInsert.execute([
            "k"       : "cpu-load",
            "v"       : activeDevices.cpuLoad,
            "occurred": new Date(),
            "ret"     : "stats-cpu-load-${stat}"
        ])


        jdbcInsert.execute([
            "k"       : "mem-load",
            "v"       : activeDevices.memoryLoad,
            "occurred": new Date(),
            "ret"     : "stats-mem-load-${stat}"
        ])


        availableWorkload.remove(0)

    }
}
