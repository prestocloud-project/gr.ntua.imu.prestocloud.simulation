package gr.ntua.imu.prestocloud.simulation.services

import gr.ntua.imu.prestocloud.simulation.configuration.ApplicationConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-05-15.
 */
@Service
class WorkloadCalculatorService {


    @Autowired
    ApplicationConfiguration applicationConfiguration

    @Value('${application.workloadPerCPU}')
    double workloadPerCPU

    @Value('${application.workloadPerMem}')
    double workloadPerMem



    def currentCpuLoad(workload,cpu,fragment){

        def cpuWorkload = workload * (1 - fragment.cpuMemRatioPerWorkload)
        def cpuLoad = cpuWorkload / (cpu * workloadPerCPU)

        if(cpuLoad < 0 ){
            cpuLoad= 0
        }

        return cpuLoad

    }


    def currentMemLoad(workload,mem,fragment){

        def memWorkload = workload * fragment.cpuMemRatioPerWorkload

        def memLoad = memWorkload / (mem * workloadPerMem)

        if(memLoad < 0 ){
            memLoad= 0
        }

        return memLoad


    }



    def currentCpuCapacity(cpu) {
        return (cpu * workloadPerCPU)
    }


    def currentMemCapacity(mem) {
        return (mem * workloadPerMem)
    }


}
