package gr.ntua.imu.prestocloud.simulation.services

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Service

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-07-10.
 */
@Service
class StatsService {

    Logger logger = LoggerFactory.getLogger(StatsService.class)

    def stats
    def enabled

    public StatsService(){

        enabled=true
        stats =[
            "numberOfAdaptations" : 0,
            "scaleIn" : 0,
            "scaleOut" : 0,
            "maxScaleOutInstances":0,
            "maxScaleInInstances":0,
            // how much time between adaptations to test cooling down period 
        ]
    }


    public void recordIncoming(def o, SimpleJdbcInsert db){

        logger.debug("Recording incoming {} ",o)

        stats.numberOfAdaptations++


        if("topology_adaptation.scale_out".equals(o.topic)){
            stats.scaleOut++

            stats.maxScaleOutInstances = Math.max(stats.maxScaleOutInstances, o.payload.delta_instances)
        }


        if("topology_adaptation.scale_in".equals(o.topic)){
            stats.scaleIn++
            stats.maxScaleInInstances= Math.max(stats.maxScaleInInstances, o.payload.delta_instances)
        }


        db.execute([
            "k"       : "number-adaptations",
            "v"       : stats.numberOfAdaptations,
            "occurred": new Date(),
            "ret"     : "stats-number-adaptations"
        ])

        db.execute([
            "k"       : "number-scale-out",
            "v"       : stats.scaleOut,
            "occurred": new Date(),
            "ret"     : "number-scale-out"
        ])

        db.execute([
            "k"       : "number-scale-in",
            "v"       : stats.scaleIn,
            "occurred": new Date(),
            "ret"     : "number-scale-in"
        ])

        db.execute([
            "k"       : "number-max-scale-out",
            "v"       : stats.maxScaleOutInstances,
            "occurred": new Date(),
            "ret"     : "number-max-scale-out"
        ])


        db.execute([
            "k"       : "number-max-scale-in",
            "v"       : stats.maxScaleInInstances,
            "occurred": new Date(),
            "ret"     : "number-max-scale-in"
        ])


    }

    public void printStats(){

        enabled =false
        logger.info("Current Stats \n\n{}\n\n",stats)
    }
}
