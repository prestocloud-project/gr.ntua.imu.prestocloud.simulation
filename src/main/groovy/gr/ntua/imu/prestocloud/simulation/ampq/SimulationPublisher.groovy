package gr.ntua.imu.prestocloud.simulation.ampq

import com.google.gson.Gson
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import producer.AmqpProducer

import javax.annotation.PostConstruct

@Service
public class SimulationPublisher {


    private String EXCHANGE_NAME = "presto.cloud";

    Logger logger = LoggerFactory.getLogger(SimulationPublisher.class)

    @Autowired
    AmqpProducer producer

    Gson gson
    File f
    FileOutputStream stream

    @PostConstruct
    void postConstruct() {

        f = new File('/tmp/recal.csv')

        if (f.exists()) {
            f.delete()
        }

        f.createNewFile()
        stream = new FileOutputStream(f, true)
        gson = new Gson()

    }


    void publish(String topic, Object message) {


        String j = gson.toJson(message)

        IOUtils.write(new Date().toString() + ",\"" + j + "\"\n", stream, "UTF-8")

        logger.trace("Publishing {} -> {} ", topic, j)


        try {

            producer.publish(
                j,
                topic
            )
        } catch (Exception e) {
            logger.error("Error sending message {}->{} ", topic, j)
        }





    }


}