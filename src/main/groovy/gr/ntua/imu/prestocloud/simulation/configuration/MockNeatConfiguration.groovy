package gr.ntua.imu.prestocloud.simulation.configuration

import net.andreinc.mockneat.MockNeat
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-12-17.
 */
@Configuration
class MockNeatConfiguration {


    @Bean
    public MockNeat mockNeat(){
        return MockNeat.threadLocal()
    }
}
