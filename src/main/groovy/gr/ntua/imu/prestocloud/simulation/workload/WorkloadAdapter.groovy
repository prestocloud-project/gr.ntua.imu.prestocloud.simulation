package gr.ntua.imu.prestocloud.simulation.workload

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-07-24.
 */
interface WorkloadAdapter {

    def List workload()
    def String key()
}
