package gr.ntua.imu.prestocloud.simulation.services

import gr.ntua.imu.prestocloud.simulation.ampq.SimulationPublisher
import gr.ntua.imu.prestocloud.simulation.configuration.ApplicationConfiguration
import gr.ntua.imu.prestocloud.simulation.workload.WorkloadAdapter
import net.andreinc.mockneat.MockNeat
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-12-05.
 */
@Service
class TopologyService {

    Logger logger = LoggerFactory.getLogger(TopologyService.class)

    @Autowired
    SimulationPublisher publisher

    @Autowired
    ApplicationConfiguration applicationConfiguration

    @Autowired
    WorkloadCalculatorService workloadCalculatorService

    @Autowired
    DevicesService devicesService

    @Autowired
    JdbcTemplate jdbcTemplate

    @Autowired
    MockNeat mockNeat

    SimpleJdbcInsert jdbcStatsInsert
    SimpleJdbcInsert jdbcInsert


    @PostConstruct
    public post() {

        jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
            .withTableName("topology")
            .usingGeneratedKeyColumns("id")


        jdbcStatsInsert = new SimpleJdbcInsert(jdbcTemplate)
            .withTableName("topology_stats")
            .usingGeneratedKeyColumns("id")



//        jdbcTemplate.update("truncate topology RESTART IDENTITY;")


        applicationConfiguration.topology.each {
            k, v ->

                for(int i = 0 ; i < v.instances; i ++){

                    def device = devicesService.getDevice(v.type)
                    def instance = mockNeat.hashes().md2().get()
                    devicesService.reserveDevice(
                        device.id,
                        k,
                        instance
                    )
                }

                jdbcInsert.execute([
                    "fragid":k,
                    "type":v.type,
                    "instances": v.instances,
                    "updated":new Date()
                ])
        }
    }


    def Map<String, Object> currentTopology() {


        def fragments = jdbcTemplate.queryForList(
            "select distinct fragid from topology"
        )


        def live = [] as Map
        fragments.each{
            f ->
                live.put(f, currentTopologyForFragment(f))
        }

        return live

    }

    def Map currentTopologyForFragment(String f){
        return jdbcTemplate.queryForMap("select * from topology where fragid=? order by updated desc limit 1 ",f)
    }


    def adjustInstances(double instances, String f) {

        def fragment = applicationConfiguration.topology.get(f)
        def topology = currentTopologyForFragment(f)

        if(instances < 0){

            for( int i = instances; i <0; i++ ){
                devicesService.releaseDevice(f)
            }

        }else {

            for ( int i = 0 ; i < instances; i++){

                def device = devicesService.getDevice(fragment.type)

                if(device !=null){

                    devicesService.reserveDevice(device.id,f, mockNeat.hashes().md2().get())
                }else{
                    logger.error("Not enough devices left for {} type ",fragment.type)
                }
            }

        }

        def previousInstances = topology.instances
        def numberOfInstances = previousInstances+instances
        if (numberOfInstances < 1) {
            numberOfInstances = 1
        }

        jdbcInsert.execute([
            "fragid":f,
            "type":fragment.type,
            "instances": numberOfInstances,
            "updated":new Date()
        ])


        publisher.publish("deployment.ack",new Object())
        logger.info("Setting {} instances from {} + {} = {} ",f, previousInstances, instances, numberOfInstances)

    }



}
