package gr.ntua.imu.prestocloud.simulation.services

import net.andreinc.mockneat.unit.seq.Seq
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2020-01-03.
 */
@Service
class SequenceStepService {

    Logger logger = LoggerFactory.getLogger(SequenceStepService.class)

    public final static int STEP_BENCHMARK = 0
    public final static int STEP_TOPOLOGY = 1
    public final static int STEP_INITIAL_DEPLOYMENT = 2
    public final static int STEP_POLICY = 3
    public final static int STEP_WAIT = 4
    public final static int STEP_READY = 5
    public final static int STEP_SENT = 6
    public final static int STEP_WAITING = 7
    public final static int STEP_PROCESSED = 8

    int step = STEP_BENCHMARK

    public int currentStep(){
        return this.step

    }

    public boolean isStep(int step){
        return this.step == step
    }


    public void nextStep(){
        step ++

        if(step > STEP_PROCESSED){
            step=STEP_PROCESSED
        }

        logger.debug("Current step {} ",step)


    }


    public void previousStep(){
        step --

        if(step <=0){
            step = STEP_BENCHMARK
        }
    }


}
