package gr.ntua.imu.prestocloud.simulation

import gr.ntua.imu.prestocloud.simulation.processors.ComputeCurrentWorkloadProcessor
import gr.ntua.imu.prestocloud.simulation.services.InitializationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled

@SpringBootApplication
@EnableScheduling
class PrestocloudSimulationApplication implements CommandLineRunner {

    @Autowired
    ComputeCurrentWorkloadProcessor computeCurrentWorkloadProcessor

    @Autowired
    InitializationService initializationService


	static void main(String[] args) {
		SpringApplication.run(PrestocloudSimulationApplication, args)
	}


    @Scheduled(fixedDelayString = '${application.time.workloadHandleInternalInMillis}')
    void run() throws Exception {
        computeCurrentWorkloadProcessor.process()
    }

    @Override
    void run(String... args) throws Exception {

        initializationService.init()

    }
}
