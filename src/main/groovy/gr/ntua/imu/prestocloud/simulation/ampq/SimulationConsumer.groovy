package gr.ntua.imu.prestocloud.simulation.ampq

import com.google.gson.Gson
import gr.ntua.imu.prestocloud.simulation.services.ActionHandlingService

import java.util.function.BiConsumer

public class SimulationConsumer implements BiConsumer<String,String> {


    private final ActionHandlingService actionHandlingService

    Gson gsonizer

    public SimulationConsumer(ActionHandlingService actionHandlingService){
        this.actionHandlingService = actionHandlingService
        this.gsonizer = new Gson()

    }


    @Override
    void accept(String body, String topic) {

        def o = gsonizer.fromJson(body,Object.class)
        actionHandlingService.handleMessage([
            topic: topic,
            payload: o]
        )

    }
}