package gr.ntua.imu.prestocloud.simulation.workload

import com.google.gson.Gson

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-07-24.
 */
class AppleWorkload implements WorkloadAdapter{


    String key

    int multiplier

    public AppleWorkload(int multiplier){
        this.multiplier = multiplier
    }

    @Override
    List workload() {

        def o = new Gson().fromJson(
            new InputStreamReader(AppleWorkload.class.getResourceAsStream("/stock/aapl-1m-full.json"))
            , Map.class)


        def memoryWorkload = o["Time Series (1min)"]
        key = o["Meta Data"]["2. Symbol"]

        def max= 0
        def min= Integer.MAX_VALUE
        def normalizedWorkload = []

        memoryWorkload.each {
            e ->

                def close = Double.valueOf(e.value["4. close"]).toInteger()
                max = Math.max(max, close)
                min = Math.min(min,close)
        }


        memoryWorkload.each {
            e->
                def normalized = (Double.valueOf(e.value["4. close"]) - min)*100


                if(normalized < 0){
                    normalized=0
                }

                normalizedWorkload.push( normalized *multiplier )
        }


        return normalizedWorkload


    }

    @Override
    String key() {
        return this.key
    }
}
