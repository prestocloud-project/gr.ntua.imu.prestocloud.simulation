package gr.ntua.imu.prestocloud.simulation.services

import com.google.gson.Gson
import gr.ntua.imu.prestocloud.simulation.configuration.ApplicationConfiguration
import org.apache.commons.lang3.StringUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 05/10/18.
 */

@Service
class ActionHandlingService {

    Logger logger = LoggerFactory.getLogger(ActionHandlingService.class)

    @Autowired
    Gson gsonizer


    @Autowired
    JdbcTemplate jdbcTemplate


    @Autowired
    StatsService statsService

    SimpleJdbcInsert jdbcInsert
    SimpleJdbcInsert annotationsInsert


    @Autowired
    ApplicationConfiguration applicationConfiguration

    @Autowired
    TopologyService topologyService

    ExecutorService executorService

    @Autowired
    SequenceStepService sequenceStepService

    @Autowired
    DevicesService devicesService

    Map<String, Object > events

    @PostConstruct
    def post(){

        events=[
            "topology_adaptation": null,
            "topology_adaptation.excluded_devices.ack": null,
            "deployment.req": null
        ]

        executorService = Executors.newFixedThreadPool(4)

        jdbcInsert = new SimpleJdbcInsert(jdbcTemplate)
            .withTableName("stats")
            .usingGeneratedKeyColumns("id")

        annotationsInsert = new SimpleJdbcInsert(jdbcTemplate)
            .withTableName("annotations")
            .usingGeneratedKeyColumns("id")


    }

    void handleMessage(Object message){


        if(message == null){
            return
        }


        if( !(message instanceof Collection)){
            message = [message]
        }

        for(def m : message.iterator()){

            try {

                if(m.topic == "topology_adaptation.excluded_devices.ack"){

                    //get from queue add to queue
                    if(events.get("topology_adaptation.excluded_devices.ack") !=null){

                        logger.error("We really screwed up here...topology_adaptation.excluded_devices.ack")
                    }

                    events.put("topology_adaptation.excluded_devices.ack",m)

                    def exclussion = events.get('topology_adaptation.excluded_devices.ack')
                    devicesService.unexclude()
                    exclussion.payload.each{
                        e ->
                            e.excluded_devices.each{
                                ip ->
                                    devicesService.excludeDevice(ip)
                            }
                    }

                }

                if(sequenceStepService.isStep(SequenceStepService.STEP_TOPOLOGY) && m.topic == "deployment.req"){
                    sequenceStepService.nextStep()
                    return
                }

                if(sequenceStepService.currentStep() < SequenceStepService.STEP_READY){
                    logger.debug("We are not ready yet {} ",sequenceStepService.currentStep())
                    return
                }

                if(StringUtils.startsWith(m.topic as String,"topology_adaptation.scale")){

                    if(events.get("topology_adaptation") !=null){

                        logger.error("We really screwed up here...")
                    }

                    //add to queue
                    logger.debug("Received adaptation event adding to queue topology_adaptation")
                    events.put("topology_adaptation",m)

                }




                if(m.topic == "deployment.req"){
                    //get from queue add to queue
                    if(events.get("deployment.req") !=null){

                        logger.error("We really screwed up here...deployment.req")
                    }
                    //get from queu deploy
                    events.put("deployment.req",m)
                }


                if(

                    events.get('topology_adaptation') != null &&
                    events.get('topology_adaptation.excluded_devices.ack') != null &&
                    events.get('deployment.req') != null

                ){

                    def adaptation = events.get('topology_adaptation')
                    def text = "Topic: ${adaptation.topic}\n Scale By: ${adaptation.payload.delta_instances}".toString();

                    logger.debug("Stamping adaptation: {}",text)
                    annotationsInsert.execute([
                        "action"     : adaptation.topic+"-"+adaptation.payload.fragment_name,
                        "text"       : text.toString(),
                        "occurred": new Date()
                    ])

                    def multiplier = 1
                    if(StringUtils.indexOf(adaptation.topic,"topology_adaptation.scale_in") >= 0){
                        multiplier = -1
                    }

                    topologyService.adjustInstances(adaptation.payload.delta_instances*multiplier,adaptation.payload.fragment_name)
                    statsService.recordIncoming(adaptation,jdbcInsert)

                    logger.trace("{}",adaptation)

                    events = [
                        "topology_adaptation": null,
                        "topology_adaptation.excluded_devices.ack": null,
                        "deployment.req": null
                    ]

                }else{
                    logger.debug("We are waiting for the other events to come in... ")
                }


            } catch (Exception e) {

                e.printStackTrace()

            } finally {
            }
        }

    }
}
