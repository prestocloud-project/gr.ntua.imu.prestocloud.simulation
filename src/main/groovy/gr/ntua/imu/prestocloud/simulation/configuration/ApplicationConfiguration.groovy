package gr.ntua.imu.prestocloud.simulation.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 2019-03-13.
 */
@Configuration
@ConfigurationProperties(prefix = "application")
class ApplicationConfiguration {

    String from
    String to
    String stock

    int adapterMultiplier=30

    Map<String, Object> devices

    Map<String, Object> topology

    Map<String, String> graph

}
