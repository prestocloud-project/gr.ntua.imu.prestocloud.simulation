package gr.ntua.imu.prestocloud.simulation.configuration

import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import consumer.AmqpConsumer
import gr.ntua.imu.prestocloud.simulation.ampq.SimulationConsumer
import gr.ntua.imu.prestocloud.simulation.ampq.SimulationReceiver
import gr.ntua.imu.prestocloud.simulation.services.ActionHandlingService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import producer.AmqpProducer

/**
 * If no license is here then you can whatever you like!
 * and of course I am not liable
 *
 * Created by fotis on 04/10/18.
 */
@Configuration
@ConfigurationProperties("spring.rabbitmq")
class RabbitMQConfiguration {

    private String EXCHANGE_NAME = "presto.cloud";

    @Autowired
    ActionHandlingService actionHandlingService

    String host
    String port
    String username
    String password

    Logger logger = LoggerFactory.getLogger(RabbitMQConfiguration.class)


    @Bean
    public AmqpProducer producer(){
        AmqpProducer producer = new AmqpProducer(
            host,
            false
        )

        producer.setUsernameAndPassword(username,password)

        return producer
    }



    @Bean
    public Map<String,AmqpConsumer> consumers(){

        Map<String,AmqpConsumer> consumers = new LinkedHashMap<>()

        def simulationConsumer = new SimulationConsumer(
            actionHandlingService
        )

        consumers.put("topology_adaptation.scale_out",
            new AmqpConsumer(
                host,
                false,
                "topology_adaptation.scale_out",
                simulationConsumer
            ))
        consumers.put("topology_adaptation.scale_in",
            new AmqpConsumer(
                host,
                false,
                "topology_adaptation.scale_in",
                simulationConsumer
            ))

        consumers.put("topology_adaptation.excluded_devices.ack",
            new AmqpConsumer(
                host,
                false,
                "topology_adaptation.excluded_devices.ack",
                simulationConsumer
            ))


        consumers.put("deployment.req",
            new AmqpConsumer(
                host,
                false,
                "deployment.req",
                simulationConsumer
            ))


        consumers.each{
            k,consumer->
                consumer.setUsernameAndPassword(username,password)
                consumer.subscribe()

        }

    }



    public ConnectionFactory factory(){


        ConnectionFactory factory = new ConnectionFactory()
        factory.setUsername(username)
        factory.setPassword(password)
        factory.setHost(host)

        return factory
    }

    public Channel channel(ConnectionFactory factory){



        Connection connection = factory.newConnection()
        Channel channel = connection.createChannel()


        try {
            channel.exchangeDeclare("presto.cloud", "topic",true) //durable=true
        } catch (Exception e) {
            channel.exchangeDeclare("presto.cloud", "topic")
        }

        String queueName = channel.queueDeclare().getQueue()
        channel.queueBind(queueName, "presto.cloud", "topology_adaptation.scale_out")
        channel.queueBind(queueName, "presto.cloud", "topology_adaptation.scale_in")

        def receiver = new SimulationReceiver(channel,actionHandlingService)
        channel.basicConsume(queueName, true, receiver)
        channel

    }


    Channel publishChannel(ConnectionFactory factory){

        def connection = factory.newConnection();
        Channel channel = connection.createChannel();

        try {
            channel.exchangeDeclare(EXCHANGE_NAME, "topic", true); //22-8-18 (last variable : durable )
        } catch (Exception e) {
            channel.exchangeDeclare(EXCHANGE_NAME, "topic", false); //22-8-18 (last variable : durable )
        }

        return channel

    }


}
